<?php

use Illuminate\Database\Seeder;

use Faker\Factory;

class BookmarkTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
       $faker = Factory::create();
        
        for( $i = 0; $i < 5; $i++ ) {
            
            DB::table('bookmarks')->insert([
                
                'name' => 'bookmarks-url-'.$i,
                'description' => $faker->realText(),
                'type' => 'url',
                'url' => $faker->url(),
                'group_id' => rand(1, 10),
                'user_id' => rand(1,10),
                'nb_votes' => rand(1,100),
                'nb_clics' => rand(1,100)
                
            ]);
            
        }
        
        for( $i = 0; $i < 5; $i++ ) {
            
            DB::table('bookmarks')->insert([
                
                'name' => 'bookmarks-text-'.$i,
                'description' => $faker->realText(),
                'type' => 'text',
                'content' => $faker->realText( 2000 ),
                'group_id' => rand(1, 10),
                'user_id' => rand(1,10),
                'nb_votes' => rand(1,100),
                'nb_clics' => rand(1,100)
                
            ]);
            
        }
        
    }
}
