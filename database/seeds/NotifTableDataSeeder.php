<?php

use Illuminate\Database\Seeder;

class NotifTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        
        for ( $i = 0; $i < 6; $i++ ) {
            
            DB::table('notifications')->insert([
                
                'user_id' => $i,
                'type' => 'like',
                'message' => 'Quelqu\'un à aimé votre bookmarks',
                'object_id' => rand(1, 10)
                
            ]);
            
        }
        
        for ( $i = 6; $i < 8; $i++ ) {
            
            DB::table('notifications')->insert([
                
                'user_id' => $i,
                'type' => 'comment',
                'message' => 'Quelqu\'un à commenté votre bookmarks',
                'object_id' => rand(1, 10)
                
            ]);
            
        }
        
        for ( $i = 8; $i < 10; $i++ ) {
            
            DB::table('notifications')->insert([
                
                'user_id' => $i,
                'type' => 'message',
                'message' => 'Quelqu\'un vous envoyé un message',
                'object_id' => rand(1, 10)
                
            ]);
            
        }
        
    }
}
