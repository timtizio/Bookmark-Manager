@extends('template')

@section('content')



@include('errors')


    <div class="panel panel-default">

        <div class="panel-heading">S'inscrire</div>


        <div class="panel-body">
            {!! Form::open( [ 'methode' => 'POST', 'route' => 'register' ] ) !!}
            
            <div class="form-group">
                {{ Form::label('name', "Nom", ['class' => 'control-label']) }}
                {{ Form::text('name', null,  ['class' => 'form-control']) }}
            </div>
            
            <div class="form-group">
                {{ Form::label('email', "Adresse Email", ['class' => 'control-label']) }}
                
                {{ Form::email('email', null, ['class' => 'form-control']) }}
            </div>
            
            <div class="form-group">
                {{ Form::label('password', "Mot de passe", ['class' => 'control-label']) }}
                {{ Form::password('password', ['class' => 'form-control']) }}
            </div>

            <div class="form-group">
                {{ Form::label('password_confirmation', "Confirmation du mot de passe", ['class' => 'control-label']) }}
                {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
            </div>
            
            

            <div class="form-group">
                {{ Form::submit("S'inscrire", ['class' => 'btn btn-primary']) }}
            </div>
            
            
            {!! Form::close() !!}
        </div>
    </div>
@stop