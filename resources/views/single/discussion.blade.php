<?php
use App\Group;
use App\Discussion;
$user = Auth::user();

$group = $discussion->group()->first();
$users = $discussion->users()->get();
$user_discussions = $user->discussions()->get();
$messages = $discussion->messages()->get();
$group_dsc = $group->discussion()->first();

?>


@extends('template')

@section('aside-no-fix')

<!-- Action discussions -->
<div class="panel panel-danger">
    
    <div class="panel-heading">
        
        <h2> Discussions </h2>
        
    </div>
    
    <div class="panel-body">
        
        <div class="list-group">
            
            <a href="{{ route( 'group-single', $group->id ) }}" class="list-group-item">Groupe</a>
            
            <a href="{{ route( 'list-users-discussion', $discussion->id ) }}" class="list-group-item">Inviter un utilisateur</a>

        </div>
        
    </div>
    
</div>

<!-- Liste des membres dans la discussion en cour -->
<div class="panel panel-warning">
    
    <div class="panel-heading">
        
        <h2> Users de la discussion </h2>
        
    </div>
    
    <div class="panel-body">
        
        <div class="list-group">
            @foreach( $users as $user )
            
                <a href="#" class="list-group-item">{{ $user->name }}</a>
                    
            @endforeach
        </div>
        
    </div>
    
</div>

<!-- Si c'est un groupe personnel alors on affiche ses discussions -->
@if ( Group::isPersonalGroup( $group->id ) )
    <!-- Mes discussions -->
    <div class="panel panel-success">

        <div class="panel-heading">

            <h2> Mes discussions </h2>

        </div>

        <div class="panel-body">

            <div class="list-group">
            @foreach( $user_discussions as $discussion )
            
            <a href="{{ route( 'discussion-show', $discussion->id ) }}" class="list-group-item">{{ $discussion->name }}</a>
                    
            @endforeach
            </div>   

        </div>

    </div>
@else
    <!-- Conversation du groupe -->
    <div class="panel panel-success">

        <div class="panel-heading">

            <h2> Converstation du groupe </h2>

        </div>

        <div class="panel-body">

            <div class="list-group">
            
            <a href="{{ route( 'discussion-show', $group_dsc->id ) }}" class="list-group-item">{{ $group_dsc->name }}</a>
                    
            </div>   

        </div>

    </div>
@endif

@endsection


@section('content')

<div class="panel panel-primary">
    
    <div class="panel-heading">

        <h1> {{ $discussion->name }} </h1>
        
    </div>
    
    
    <div class="panel-body">
        @if ( Discussion::isEmpty( $discussion->id ) )
                <p> Il n'y a pas de messages. </p>
        @else
            @foreach( $messages as $message )
                <div class="msg">
                    <p style="text-decoration: underline;">De : {{ $message->user()->first()->name }} </p>
                    <p>Message : {{ $message->message }} </p>
                    <span class="time-right">Le : {{ $message->created_at }}</span>
                </div>
            @endforeach
        @endif
    </div>
    
    <div class="panel-footer">
        
        {!! Form::open([ 'method' => 'POST', 'route' => [ 'message-store', $discussion->id ] ]) !!}
        <div class="form-group">
                {{ Form::label('subject-msg', "Entrez le sujet de votre message", ['class' => 'control-label']) }}
                {{ Form::text('subject-msg', null ,  ['class' => 'form-control']) }}
        </div>
        <textarea name="message-msg" class="form-control" rows="3"></textarea><br>
        <button type="submit" class="btn btn-success">Envoyer</button>
        
        {!! Form::close() !!}
        
        
    </div>
    
</div>

@endsection


