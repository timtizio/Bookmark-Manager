

@extends('template')


@section('content')
    
<?php

$the_tags = [];
foreach ($tags as $tag) {
    $the_tags[ $tag->id ] = $tag->name;
}
$the_categories = [];
foreach ($categories as $category) {
    $the_categories[ $category->id ] = $category->name;
}



if( isset($bookmark) ) {
    $type = "Mettre à jour";
    $options = [ 'route' => [ 'bookmark-update', $bookmark->id ] ];

    $hidden = '<input type="hidden" name="_method" value="PUT">';

    $type_bm = ( $bookmark->type === 'url' ) ? 'url' : 'text';
    $bookmark_taxos = $bookmark->taxonomies->transform( function ($item, $key) { return $item['id']; })->toArray();

}
else {

    $bookmark = new \App\Bookmark;
    $hidden = "";

    $type = "Ajouter";
    $options = ['methode' => 'POST', 'route' => 'bookmark-store'];

    $type_bm = 'url';

    $bookmark_taxos = [];
}

?>

@include('errors')


    <div class="panel panel-default">

        <div class="panel-heading">{{ $type }} un bookmark</div>


        <div class="panel-body">
            {!! Form::open($options) !!}

            {!! $hidden !!}
            

            
            <div class="form-group">
                {{ Form::label('name-bm', "Nom du Bookmark", ['class' => 'control-label']) }}
                {{ Form::text('name-bm', $bookmark->name,  ['class' => 'form-control']) }}
            </div>
            
            <div class="form-group">
                {{ Form::label('description-bm', "Description", ['class' => 'control-label']) }}
                {{ Form::text('description-bm', $bookmark->description,  ['class' => 'form-control']) }}
            </div>
            <div class="form-group">
                {{ Form::label('tags-bm', 'Tags', ['class' => 'control-label']) }}
                {{ Form::select( 'tags-bm[]', $the_tags, $bookmark_taxos, ['id' => 'tags-bm', 'multiple' => 'multiple'] ) }}
            </div>
            <div class="form-group">
                {{ Form::label('categories-bm', 'Catégorie', ['class' => 'control-label']) }}
                {{ Form::select( 'categories-bm[]', $the_categories, $bookmark_taxos, ['class' => 'form-control'] ) }}
            </div>


            <div class="form-group">
                {{ Form::label('type-bm', "Type de Bookmark", ['class' => 'control-label']) }}
                
                {{ Form::select('type-bm', ['url' => 'URL', 'text' => 'Texte'], $type_bm, ['class' => 'form-control']) }}
            </div>
            
            <div id="type">
                <div class="form-group" id="form-bm-url">
                    {{ Form::label('url-bm', "Url", ['class' => 'control-label']) }}
                    {{ Form::text('url-bm', $bookmark->url,  ['class' => 'form-control']) }}
                </div>

                <div class="form-group cache" id="form-bm-content">
                    {{ Form::label('content-bm', "Contenu", ['class' => 'control-label']) }}
                    {{ Form::textarea('content-bm', $bookmark->content,  ['class' => 'form-control']) }}
                </div>
            </div>
            
            
            

            <div class="form-group">
                {{ Form::submit($type, ['class' => 'btn btn-primary']) }}
            </div>
            
            
            {!! Form::close() !!}
        </div>
    </div>
@stop
{{-- ****************************************************************************************************** --}}
@section('styles')

<link href="/css/bootstrap-multiselect.css" media="screen" rel="stylesheet" type="text/css">

@stop
{{-- ****************************************************************************************************** --}}
@section('scripts')
<script src="/js/bootstrap-multiselect.js" type="text/javascript"></script>

<script>
    $('#tags-bm').multiselect({ enableFiltering: true });
    $('#tags-bm').addClass('cache');


    $( function(){
        changeClass();
        $( "#type-bm" ).on( "change", function() {
            changeClass();
        });
    });
    


    function changeClass() {
        if( $( "#type-bm" ).val() === "url" ) {
            $("#type").children("#form-bm-content").removeClass('visible').addClass('cache');
            $("#type").children("#form-bm-url").removeClass('cache').addClass('visible');
        }
        else { 
            $( "#type" ).children( "#form-bm-url" ).removeClass( 'visible' ).addClass( 'cache' );
            $( "#type" ).children( "#form-bm-content" ).removeClass( 'cache' ).addClass( 'visible' );
        }
    }
    
    
</script>


@stop
{{-- ****************************************************************************************************** --}}




<?php

function isSelected( $id_taxo, $bookmark ) {

    $val_return = "";
    if( isset( $bookmark ) ) {

        $taxos_bookmark = $bookmark->taxonomies->transform( function ($item, $key) { return $item['id']; })->toArray();
        // dd($id_taxo);
        if( in_array( $id_taxo, $taxos_bookmark ) ) $val_return = 'selected=selected';
    }
    return $val_return;
}

?>
