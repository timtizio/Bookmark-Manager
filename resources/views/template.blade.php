<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Bookmark-Manager</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        
        <link href="/css/app.css" rel="stylesheet" type="text/css">
        @yield('styles')
    </head>
    
    <body>
        <header class="container-fluid">
            @include('navbar')
        </header>
        
        <div id="container" class="container-fluid bg-danger">
            <div class="row">
                <div id="aside-no-fix" class="col-md-2 col-sm-3">
                    <!-- Aside non-fix -->
                    @yield('aside-no-fix')
                </div>

                <div id="content" class="col-md-8 col-sm-9">
                    <!-- content -->
                     @yield('content')
                </div>

                <div id="aside-fix" class="col-md-2 hidden-sm">
                    <!-- Aside fix -->
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel, hic, neque consequuntur sint quia nostrum accusamus perferendis ea quos dicta sed laboriosam ex id aliquam distinctio necessitatibus rerum eum fugit molestias cupiditate odio minima pariatur veritatis blanditiis illo fugiat quae aperiam recusandae assumenda vero. Voluptatem, unde, reiciendis sit et incidunt molestiae sint! Commodi, minus, quos adipisci deserunt assumenda voluptate officiis ab velit odio eius modi impedit veniam culpa nisi magni suscipit animi dolores repellendus consectetur natus! Suscipit, illum, amet beatae consequatur voluptatibus non eum perspiciatis mollitia fuga minima molestiae numquam cumque deserunt! Distinctio doloribus iure amet officiis tempora exercitationem. Voluptates, fugiat quaerat culpa tempore temporibus accusamus dolore deleniti eaque harum neque distinctio repellat earum ab optio perspiciatis velit magnam sequi commodi ipsa corrupti aperiam rerum esse! Nulla, impedit, nesciunt, neque repudiandae vitae accusamus eos ea corrupti suscipit nam earum excepturi iure adipisci itaque minima alias ipsum. Hic, dolorum, minus quasi itaque animi quia dicta molestias asperiores! Totam, dignissimos cupiditate modi velit aliquam asperiores tempora nihil dolores magnam harum aliquid eveniet hic architecto consectetur saepe a ad officia in quas inventore ut debitis vero distinctio porro molestiae voluptate reprehenderit labore recusandae consequatur id? Temporibus, voluptatem, assumenda, totam delectus tempora veniam repudiandae aut porro nobis maxime adipisci earum est soluta debitis sapiente ea iste in excepturi consequatur cupiditate commodi veritatis reiciendis odio tenetur esse repellat autem! Voluptates, delectus, iure, possimus laboriosam ipsam assumenda provident inventore error minima dolore libero impedit quo nisi incidunt quidem molestias repellat facere praesentium tenetur ullam debitis nostrum repudiandae veniam. Natus, deleniti sequi doloremque enim quasi tenetur cupiditate tempore. Fuga, quas corrupti autem ratione perspiciatis ex in iste optio voluptas quam natus reiciendis placeat quo error labore tempore atque. Placeat, inventore, modi, distinctio quo similique eius nesciunt quisquam quis tempore hic deserunt reiciendis rem quia officia sapiente nisi veniam iusto eaque expedita eos cupiditate provident aspernatur. Quidem, odio, dolorum odit dolor doloribus unde corporis sed saepe suscipit molestiae obcaecati impedit iure nostrum magnam ipsum vel ipsam animi aliquam consectetur similique optio. Nisi neque praesentium culpa dignissimos quia. Quasi, quibusdam praesentium earum laudantium dolorem omnis enim. Dolorum, nostrum, officia. Numquam, explicabo amet placeat nobis labore ad exercitationem in. At, corrupti labore facere autem minus nulla! Ad, earum, odit, neque nobis ipsa vel magnam dolorum blanditiis sapiente sint doloremque sit recusandae molestias quod obcaecati autem odio quae dolores soluta at. Necessitatibus, iusto, ad, esse quo suscipit dolore error a molestiae laudantium ut voluptatum eum ipsum veritatis quos impedit rem blanditiis repellendus nesciunt quidem rerum corporis cupiditate veniam omnis repellat ullam asperiores quaerat. Beatae quo dolorum in quos minus! Ratione officiis veniam aperiam esse accusantium sint ipsum. Reiciendis, earum, molestias temporibus dolor esse voluptatum eaque quisquam impedit repellat cumque iure quos soluta est suscipit odio facilis tempora officia obcaecati ullam quo eos magnam delectus nemo dicta eius rerum maxime id repudiandae non adipisci! Mollitia, ipsum, dolorem, delectus dolores numquam id aut error perferendis nihil rem unde natus iure dolore eius debitis ut possimus fugit veniam quas quaerat maiores suscipit reiciendis aspernatur.
                </div>
            </div>
        </div>
        
        <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous">
        </script>

        <script 
            src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous">
        </script>

        <script>
            /* Pour le token de sécurité */
            $(function() {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            });
            
        </script>
        @yield('scripts')
    </body>
</html>
