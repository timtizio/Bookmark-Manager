<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Faker\Factory;
class UserTest extends TestCase
{
    
    private $faker;
    private $user;
    
    /**
     * A basic test example.
     *
     * @return void
     */
    
    public function setUp() {
        parent::setUp();
        $this->faker = Factory::create();
        
        
    }
    
    /**
     * Inscris un utilisateur, et vérifie que le compte à bien été créer et que le group perso aussi
     */
    public function testInscription() {
        
        $user = new \App\User();
        $userRepository = new \App\Repositories\UserRepository( $user );
        
        
        $name = $this->faker->name(); $email = $this->faker->email(); 

        $user = $userRepository->register( [
            'name' => $name,
            'email' => $email,
            'password' => bcrypt( 'azerty' ),
        ] );

        $group_id = $user->groups()->first()->id;
        
        $this->assertDatabaseHas( 'users', [ 'email' => $email, 'name' => $name ] );
        $this->assertDatabaseHas( 'groups', [ 'name' => $name, 'description' => 'Groupe personnel de ' . $name ]);
        $this->assertDatabaseHas( 'group_user', [ 'user_id' => $user->id, 'group_id' => $group_id ]);
    }
    
    public function testVoteForOneBookmark() {
        // On créer un User
        $user = new \App\User();
        
        $name = $this->faker->name(); $email = $this->faker->email(); 
        
        $user->name = $name;
        $user->email = $email;
        $user->password = bcrypt( 'azerty' );
        
        $user->save();
        $userRepository = new \App\Repositories\UserRepository( $user );

        
        // On créer le BM
        $bm = new \App\Bookmark();
        $bmRepository = new \App\Repositories\BookmarkRepository( $bm );
        
        $name = $this->faker->text( 30 );
        
        $id_bm = $bmRepository->save( [
            'name-bm' => $name,
            'description-bm' => $this->faker->realText(),
            'type-bm' => 'url',
            'url-bm' => $this->faker->url(),
        ] )->id;
        
        // On vérifie que le nb_vote soit bien de zéro
        $this->assertDatabaseHas( 'bookmarks', [ 
            'name' => $name,
            'nb_votes' => 0
        ]);
        
        
        // On vote un premiere fois
        $userRepository->vote_for_bookmark( $id_bm );
        $this->assertDatabaseHas( 'bookmarks', [ 'nb_votes' => 1 ] );
        
        // On vote un seconde fois
        $userRepository->vote_for_bookmark( $id_bm );
        $this->assertDatabaseHas( 'bookmarks', [ 'nb_votes' => 1 ] );
    }
    
    public function testCommentABookmark() {
        // On créer un User
        $user = new \App\User();
        
        $name = $this->faker->name(); $email = $this->faker->email(); 
        
        $user->name = $name;
        $user->email = $email;
        $user->password = bcrypt( 'azerty' );
        
        $user->save();
        $userRepository = new \App\Repositories\UserRepository( $user );

        
        // On créer le BM
        $bm = new \App\Bookmark();
        $bmRepository = new \App\Repositories\BookmarkRepository( $bm );
        
        $name = $this->faker->text( 30 );
        
        $id_bm = $bmRepository->save( [
            'name-bm' => $name,
            'description-bm' => $this->faker->realText(),
            'type-bm' => 'url',
            'url-bm' => $this->faker->url(),
        ] )->id;
        
        // L'utilisateur commente le bm
        $content = $this->faker->realText();
        $userRepository->comment_bookmark( $id_bm, $content );
        
        // on vérifie qu'on ai bien un seul commentaire
        $this->assertEquals( 1, $bmRepository->countComment() );
        $this->assertEquals( $content, $bm->comments()->first()->content );
        
        // Il recommmente , on vérifie qu'on en ai bien deux
        $content_comment2 = $this->faker->realText();
        $userRepository->comment_bookmark( $id_bm, $content_comment2 );
        $this->assertEquals( 2, $bmRepository->countComment() );
        
        
        
    }
    
}
