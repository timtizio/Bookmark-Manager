<?php


namespace App\Repositories;


use App\Message;


class MessageRepository implements MessageRepositoryInterface {


    protected $message;


    public function __construct(Message $message) {

        $this->message = $message;

    }


    /**
     * Enregistre un Message
     * 
     * @param  array
     * @return mixed bool | App\Message
     */
    public function save( $inputs ) {

        $this->message->subject = $inputs[ 'subject-msg' ];
        $this->message->message = $inputs[ 'message-msg' ];
        $this->message->user_id = $inputs[ 'user-id' ];
        $this->message->save();
        
        $this->message->discussions()->attach( $inputs[ 'discussion-id' ] );
        $this->message->discussions()->attach( $inputs[ 'user-id' ] );
        
        
        return $this->message;

    }

}