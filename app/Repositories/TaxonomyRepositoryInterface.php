<?php


namespace App\Repositories;


interface TaxonomyRepositoryInterface {

    public function save( $bookmark, $args );

}