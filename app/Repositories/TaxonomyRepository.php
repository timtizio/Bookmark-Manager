<?php


namespace App\Repositories;


use App\Taxonomy;


class TaxonomyRepository implements TaxonomyRepositoryInterface {


    protected $taxonomy;


    public function __construct(Taxonomy $taxonomy) {

        $this->taxonomy = $taxonomy;

    }


    /**
     * Enregistre une taxonomy
     * 
     * @param  array
     * @return mixed bool | App\Taxonomy
     */
    public function save( $bookmark, $id_taxonomies ) {

        $bookmark->taxonomies()->sync( $id_taxonomies );



    }

}