<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function users() {
        
        return $this->belongsToMany('App\User', null, 'group_id', 'user_id');
        
    }
    
    public function discussion() {
        
        return $this->hasOne('App\Discussion');
        
    }
    
    public function bookmarks() {
        
        return $this->hasMany('App\Bookmark');
        
    }
    
    public function isAdmin($id_user) {
        $role = GroupUser::where('group_id', $this->id)->where('user_id', $id_user)->get()->first();
        
        if( is_null( $role ) ) return false;
        
        return GroupUser::where('group_id', $this->id)->where('user_id', $id_user)->get()->first()->role === 'admin';
        
    }
    
    public function isMember($id_user) {
        
         return GroupUser::where('group_id', $this->id)->where('user_id', $id_user)->get()->first()->role === 'member';
        
    }
    
    public function isFollower($id_user) {
        
         return GroupUser::where('group_id', $this->id)->where('user_id', $id_user)->get()->first()->role === 'follower';
        
    }
    
    public function role_user_group($id_user) {
        
        return GroupUser::where('group_id', $this->id)->where('user_id', $id_user)->get()->first()->role;
        
    }
    
    /**
     * Verifie si $id_group est un groupe personnel
     * @param int $id_group
     * @return bool
     */
    public static function isPersonalGroup($id_group) {
        
        $group = Group::findOrFail( $id_group );
        $nb_users = count( $group->users()->get() );
        
        $group_status = ( 'private' === $group->visibility && 1 === $nb_users ) ? true : false;
        
        return $group_status;
        
    }
}
