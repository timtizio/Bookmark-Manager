<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Support\Facades\Auth;

class BookmarkRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return Auth::guard()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules =  [
            'name-bm' => 'bail|required|string|max:50',
            'type-bm' => ['bail', 'required', 'regex:/^url$|^text$/'],
            'description-bm' => 'bail|required|string|max:255'
        ];

        // On rajoute la règle qui va bien en fonction de la valeur de url-bm
        if( $_POST['type-bm'] === 'url' ) 
            $rules['url-bm'] = 'URL';
        elseif( $_POST['type-bm'] === 'text' ) 
            $rules['content-bm'] = 'bail|string|max:16383';
        
        return $rules;
    }
}
