<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookmarkRequest;
use App\Http\Requests\CommentRequest;
use App\Http\Requests\SearchRequest;

use Illuminate\Http\Request;

use App\Bookmark;
use App\Taxonomy;
use App\User;

use App\Repositories\BookmarkRepository;
use App\Repositories\TaxonomyRepository;
use \App\Repositories\UserRepository;

class BookmarkController extends Controller
{

    protected $bookmarkRepository;

    public function __construct( BookmarkRepository $bookmarkRepository ) {
        $this->bookmarkRepository = $bookmarkRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
        $this->authorize( 'lister', Bookmark::class );
        
        $bookmarks = Bookmark::all();

        return view( 'list.bookmark' )->with( 'bookmarks', $bookmarks );
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // On charge les tags et les categories
        $taxos = $this->get_taxos();

        return view('form.edit-bm', [ 
            'tags' => $taxos['tags'],
            'categories' => $taxos['categories']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( BookmarkRequest $request, TaxonomyRepository $taxonomyRepository )
    {
        $inputs = $request->all();
        $inputs_taxos = array_merge( $inputs['tags-bm'], $inputs['categories-bm'] );


        $bookmark = $this->bookmarkRepository->save( $inputs );

        if( isset( $inputs_taxos ) ) {
            $taxonomyRepository->save( $bookmark, $inputs_taxos );
        }

        return redirect( route('bookmark-edit', $bookmark->id) );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $bookmark = Bookmark::findOrFail( $id );

        return view( 'single.bookmark' )->with([
            'bookmark' => $bookmark,
            'comments' => $bookmark->comments()->get(),
            'canEdit' => true,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $bookmark = Bookmark::findOrFail($id);
        
        $this->authorize( 'update', $bookmark );

        // On charge les tags et les categories
        $taxos = $this->get_taxos();


        return view('form.edit-bm')->with([
            'bookmark' => $bookmark,
            'tags' => $taxos['tags'],
            'categories' => $taxos['categories']
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BookmarkRequest $request, $id, TaxonomyRepository $taxonomyRepository)
    {
        $inputs = $request->all();
        $inputs_taxos = array_merge( $inputs['tags-bm'], $inputs['categories-bm'] );
        
        $bookmark = Bookmark::findOrFail( $id );

        $bookmarkRepository = new BookmarkRepository( $bookmark );
        $bookmarkRepository->save( $inputs );


        if( isset( $inputs_taxos ) ) {
            $taxonomyRepository->save( $bookmark, $inputs_taxos );
        }
        return redirect( route( 'bookmark-edit', $id ) );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $bookmark = Bookmark::findOrfail( $id );
        $this->authorize( 'delete', $bookmark );
        
        
        $bookmark->taxonomies()->detach();
        $bookmark->delete();
        
        return redirect( route('bookmark-list') );
    }
    
    public function add_vote( Request $request ) {
        $user = User::findOrFail( $request->input( 'id_user' ) );
        $userRepository = new UserRepository( $user );
        
        $response = $userRepository->vote_for_bookmark( $request->input( 'id_bookmark' ) );
        
        return response()->json( $response );
    }

    public function add_comment( CommentRequest $request, $id ) {
        $user = User::findOrFail( $request->input( 'id_user' ) );
        $userRepository = new UserRepository( $user );
        
        
        $userRepository->comment_bookmark( $request->input( 'id_bookmark' ), $request->input( 'content-comment' ) );
        
        return redirect( route( 'bookmark-single', $id ) );
    }
    
    public function search( SearchRequest $request ) {
        $bookmarks = BookmarkRepository::search( $request->all() );
        
        return view( 'list.bookmark' )->with( [ 'bookmarks' => $bookmarks, 'patern' => $request->input('patern') ]);
    }
    
    
    private function get_taxos() {
        $taxos = Taxonomy::all();

        $tags = [];
        $categories = [];

        
        foreach ($taxos as $taxo) {
            if( $taxo->type == 'tag' ) {
                array_push( $tags, $taxo );
            }
            elseif( $taxo->type == 'category' ) {
                array_push( $categories, $taxo );
            }
        }

        return [ "tags" => $tags, "categories" => $categories ];
    }
    
    
}
